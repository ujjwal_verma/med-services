import "../styles/style.css";
import $ from "jquery";
import RevealOnScroll from "./modules/RevealOnScroll";
import MobileMenu from "./modules/MobileMenu";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks"
import Modal from "./modules/Modal"
if(module.hot){
    module.hot.accept();
}


console.log('App.js is runnning!!!!!!!!'); 



let mobileMenu = new MobileMenu();

new RevealOnScroll($(".section"), "40%");
//new RevealOnScroll($("#departments"), "40%")

new SmoothScroll();
new ActiveLinks();

new Modal();